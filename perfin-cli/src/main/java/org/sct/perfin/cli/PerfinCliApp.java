package org.sct.perfin.cli;

import org.sct.pfin.core.group.FileRecords;
import org.sct.pfin.core.report.BalanceByClassifierReport;
import org.sct.pfin.core.report.BalanceFinReport;
import picocli.AutoComplete;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.time.LocalDate;

@Command(name = "pfin", mixinStandardHelpOptions = true, version = "pfin 1.0",
        description = "pfin is a tool for operating on personal finance data",
        commandListHeading = "%nCommands:%n",
        footer = "%nSee 'pfin help <command>' to read about a specific subcommand or concept.",
        subcommands = {
                CommandLine.HelpCommand.class,
                AutoComplete.GenerateCompletion.class
        })
public class PerfinCliApp implements Runnable {

    @Option(names = {"--record-file", "-f"}, paramLabel = "FILE_NAME", defaultValue = "operations",
            description = "Journal file to read operations from", arity = "1")
    private File operationsFile;

    @Option(names = {"--since"}, paramLabel = "YYYY-MM-DD",
            description = "Include fin operations after specified date (inclusive)")
    private LocalDate since;

    @Option(names = {"--until"}, paramLabel = "YYYY-MM-DD",
            description = "Include fin operations before specified date (inclusive)")
    private LocalDate until;

    @Override
    public void run() {
        System.out.println("No subcommand was provided");
    }

    public static void main(String[] args) {
        PerfinCliApp cliApp = new PerfinCliApp();
        CommandLine cmd = new CommandLine(cliApp);

        int exitCode = cmd.execute(args);
        System.exit(exitCode);
    }

    @Command(name = "list", description = "list journal operations")
    public void list() {
        new FileRecords(
                operationsFile
        )
        .forEach(System.out::println);
    }

    @Command(name = "balance", description = "calculate balance")
    public void balance(
            @Option(names = {"--by-date"}) boolean byDate,
            @Option(names = {"--by-classifier"}, defaultValue = "") String byClassifier) {

        if(byDate) {
            System.out.println(
                    "" // todo report by date
            );
        } else if(!byClassifier.isEmpty()) {
            new BalanceByClassifierReport(
                    new FileRecords(
                            operationsFile
                    )
            ).prepare()
                    .forEach((key, value) -> System.out.printf("%15s: %15.2f%n", key, value));
        } else {
            System.out.println(
                    new BalanceFinReport(
                            new FileRecords(
                                    operationsFile
                            )
                    )
            );
        }
    }

    @Command(name = "classifier", description = "show classifier information")
    public void classifier() {
        new FileRecords(
                operationsFile
        ).classifiers()
                .forEach(System.out::println);
    }
}
