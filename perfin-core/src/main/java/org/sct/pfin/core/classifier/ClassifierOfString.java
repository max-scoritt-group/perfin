package org.sct.pfin.core.classifier;

import org.sct.pfin.core.Classifier;
import org.sct.pfin.core.util.CachingSupplier;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * A classifier represented by a string <code>"name:a1.a2.a3[.aN]"</code>
 */
public class ClassifierOfString implements Classifier {

    private final Supplier<String> validClassifierString;
    private final String uncheckedString;

    public ClassifierOfString(String classifierString) {
        this.uncheckedString = classifierString;
        this.validClassifierString = new CachingSupplier<>(
                new ValidClassifierString()
        );
    }

    @Override
    public String name() {
        return validClassifierString.get()
                .split(":", -1)[0];
        // todo how to cache the split?
    }

    @Override
    public int depth() {
        return validClassifierString.get()
                .split("\\.").length;
    }

    @Override
    public String path() {
        return validClassifierString.get()
                .split(":", -1)[1];
    }

    @Override
    public Classifier parent() {
        String s = validClassifierString.get();


        int lastSepIndex = 0;
        for(int i = s.length() - 1; i >= 0; i--) {
            if(s.codePointAt(i) == '.') {
                lastSepIndex = i;
                break;
            }
            if(s.codePointAt(i) == ':') {
                lastSepIndex = i + 1;
                break;
            }
        }
        return new ClassifierOfString(
                s.substring(0, lastSepIndex)
        );
    }

    @Override
    public String toString() {
        return uncheckedString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassifierOfString that = (ClassifierOfString) o;
        return Objects.equals(uncheckedString, that.uncheckedString);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uncheckedString);
    }

    /**
     * Validated classifier string
     */
    private class ValidClassifierString implements Supplier<String> {
        @Override
        public String get() {
            if(!(uncheckedString.split(":", -1).length == 2)
                    || Arrays.stream(uncheckedString.split("\\.", -1)).anyMatch(String::isEmpty)) {

                throw new IllegalArgumentException("Not a valid classifier string '%s'".formatted(uncheckedString));
            }
            return uncheckedString;
        }
    }
}
