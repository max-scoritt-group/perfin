package org.sct.pfin.core.report;

import org.sct.pfin.core.FinReport;
import org.sct.pfin.core.RecordGroup;
import org.sct.pfin.core.util.StreamOfIterable;

import java.math.BigDecimal;
import java.time.Month;
import java.util.Map;

public class MonthToMonthReport implements FinReport<Map<Month, BigDecimal>> {

    private final RecordGroup records;
    private final int dayOfMonth;

    public MonthToMonthReport(RecordGroup records, int dayOfMonth) {
        this.records = records;
        this.dayOfMonth = dayOfMonth;
    }

    @Override
    public Map<Month, BigDecimal> prepare() {
        return new StreamOfIterable<>(records).get()
                .filter(
                        finRecord -> finRecord.date().getDayOfMonth() < dayOfMonth
                )
                .collect(
                       new FinRecordSumBy<>(finRecord -> finRecord.date().getMonth())
                );
    }
}
