package org.sct.pfin.core.group;

import org.sct.pfin.core.FinRecord;
import org.sct.pfin.core.RecordGroup;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Group based on {@link Iterable}
 *
 * @author Max Petrov
 */
public class RecordsOfIterable implements RecordGroup {

    public static final Collector<FinRecord, ?, RecordGroup> RECORD_GROUP_COLLECTOR =
            Collector.of(
                    ArrayList<FinRecord>::new,
                    ArrayList::add,
                    (lhs, rhs) -> {lhs.addAll(rhs); return lhs;},
                    RecordsOfIterable::new
            );

    private final Iterable<FinRecord> records;

    public RecordsOfIterable(Iterable<FinRecord> records) {
        this.records = records;
    }

    public RecordsOfIterable(FinRecord... records) {
        this(List.of(records));
    }

    @Override
    public Iterable<String> classifiers() {
        return asStream()
                .map(FinRecord::classifier)
                .distinct()
                .toList();
    }

    @Override
    public Iterable<LocalDate> dates() {
        return asStream()
                .map(FinRecord::date)
                .sorted()
                .toList();
    }

    @Override
    public Stream<FinRecord> asStream() {
        return StreamSupport.stream(this.spliterator(), false);
    }

    @Override
    public RecordGroup filter(Predicate<FinRecord> condition) {
        return asStream()
                .filter(condition)
                .collect(RECORD_GROUP_COLLECTOR);
    }

    @Override
    public Iterator<FinRecord> iterator() {
        return records.iterator();
    }
}
