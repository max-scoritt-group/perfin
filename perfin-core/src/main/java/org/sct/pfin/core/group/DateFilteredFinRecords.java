package org.sct.pfin.core.group;

import org.sct.pfin.core.RecordGroup;
import org.sct.pfin.core.util.StreamOfIterable;

import java.time.LocalDate;

/**
 * Applies date filter to the underlying group
 *
 * @author Max Petrov
 */
public class DateFilteredFinRecords extends RecordsOfIterable {
    public DateFilteredFinRecords(LocalDate date, RecordGroup baseRecords) {
        super(new StreamOfIterable<>(baseRecords).get()
                .filter(r -> r.date().equals(date))
                .toList());
    }
}
