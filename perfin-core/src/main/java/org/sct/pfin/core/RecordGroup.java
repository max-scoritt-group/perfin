package org.sct.pfin.core;

import org.sct.pfin.core.group.EmptyRecordGroup;

import java.time.LocalDate;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface RecordGroup extends Iterable<FinRecord> {
    Iterable<String> classifiers();
    Iterable<LocalDate> dates();
    Stream<FinRecord> asStream();
    RecordGroup filter(Predicate<FinRecord> condition);

    RecordGroup EMPTY_RECORD_GROUP = new EmptyRecordGroup();
}
