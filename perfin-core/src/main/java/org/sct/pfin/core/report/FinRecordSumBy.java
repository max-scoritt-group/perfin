package org.sct.pfin.core.report;

import org.sct.pfin.core.FinRecord;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Stream collector to perform group-by operation on {@link FinRecord} calculating sum for each group.
 *
 * @param <K> type of the value which records are grouped by
 */
public class FinRecordSumBy<K> implements Collector<FinRecord, Map<K, BigDecimal>, Map<K, BigDecimal>> {

    private final Collector<FinRecord, ?, Map<K, BigDecimal>> collector;

    /**
     *
     * @param groupBy function to extract group value from {@link FinRecord}
     */
    public FinRecordSumBy(Function<? super FinRecord, ? extends K> groupBy) {
        this.collector = Collectors.groupingBy(
                groupBy,
                Collectors.reducing(
                        BigDecimal.ZERO,
                        fr -> BigDecimal.valueOf(fr.amount()),
                        BigDecimal::add
                ));
    }


    @Override
    public Supplier<Map<K, BigDecimal>> supplier() {
        return (Supplier<Map<K, BigDecimal>>) collector.supplier();
    }

    @Override
    public BiConsumer<Map<K, BigDecimal>, FinRecord> accumulator() {
        return (BiConsumer<Map<K, BigDecimal>, FinRecord>) collector.accumulator();
    }

    @Override
    public BinaryOperator<Map<K, BigDecimal>> combiner() {
        return (BinaryOperator<Map<K, BigDecimal>>) collector.combiner();
    }

    @Override
    public Function<Map<K, BigDecimal>, Map<K, BigDecimal>> finisher() {
        return (Function<Map<K, BigDecimal>, Map<K, BigDecimal>>) collector.finisher();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return collector.characteristics();
    }
}
