package org.sct.pfin.core.group;

import org.sct.pfin.core.FinRecord;
import org.sct.pfin.core.RecordGroup;
import org.sct.pfin.core.util.CachingSupplier;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Filter {@link RecordGroup} by classifier
 *
 * @implNote Use caching for optimization provided by {@linkplain CachingSupplier}. Not thread-safe.
 */
public class ClassifierFilteredRecords implements RecordGroup {
    private final String classifier;
    private final RecordGroup base;

    private final Supplier<RecordGroup> filtered;

    public ClassifierFilteredRecords(String classifier, RecordGroup baseRecords) {
        this.classifier = classifier;
        this.base = baseRecords;
        this.filtered = new CachingSupplier<>(
                () -> base
                        .filter(r -> r.classifier().equals(classifier))
        );
    }

    @Override
    public Iterable<String> classifiers() {
        return filtered.get().classifiers();
    }

    @Override
    public Iterable<LocalDate> dates() {
        return filtered.get().dates();
    }

    @Override
    public Stream<FinRecord> asStream() {
        return filtered.get().asStream();
    }

    @Override
    public RecordGroup filter(Predicate<FinRecord> condition) {
        return base
                .filter(r -> r.classifier().equals(classifier) && condition.test(r));
    }

    @Override
    public Iterator<FinRecord> iterator() {
        return asStream().iterator();
    }

}
