package org.sct.pfin.core.group;

import org.sct.pfin.core.FinRecord;
import org.sct.pfin.core.RecordGroup;
import org.sct.pfin.core.record.FinRecordOfString;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class FileRecords implements RecordGroup {
    private final File recordFile;

    public FileRecords(File recordFile) {
        this.recordFile = recordFile;
    }

    @Override
    public Iterator<FinRecord> iterator() {
        return new Iterator<>() {

            private final Scanner scanner;

            {
                try {
                    scanner = new Scanner(recordFile);
                } catch (FileNotFoundException e) {
                    throw new IllegalArgumentException("Cannot parse record file " + recordFile.getAbsolutePath(), e);
                }
            }

            @Override
            public boolean hasNext() {
                return scanner.hasNextLine();
            }

            @Override
            public FinRecord next() {
                return new FinRecordOfString(scanner.nextLine());
            }
        };
    }

    @Override
    public Iterable<String> classifiers() {
        return null;
    }

    @Override
    public Iterable<LocalDate> dates() {
        return null;
    }

    @Override
    public Stream<FinRecord> asStream() {
        return null;
    }

    @Override
    public RecordGroup filter(Predicate<FinRecord> condition) {
        return null;
    }
}
