package org.sct.pfin.core.util;

import java.util.ArrayList;
import java.util.function.Supplier;

/**
 * Supplier which caches the result of underlying supplier.
 * <br/>Not thread safe
 * @param <T> Type of supplied element
 */
public class CachingSupplier<T> implements Supplier<T> {
    private final ArrayList<T> cache;
    private final Supplier<T> origin;

    public CachingSupplier(Supplier<T> origin) {
        this.origin = origin;
        cache = new ArrayList<>(1);
    }

    @Override
    public T get() {
        if(cache.isEmpty()) {
            cache.add(
                    origin.get()
            );
        }
        return cache.get(0);
    }
}
