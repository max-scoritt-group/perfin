package org.sct.pfin.core.record;

import org.sct.pfin.core.FinRecord;
import org.sct.pfin.core.util.DelimitedString;

import java.time.LocalDate;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Fin record represented as a string
 */
public class FinRecordOfString implements FinRecord {

    private final DelimitedString finRecordString;

    public FinRecordOfString(String finRecordString) {
        this.finRecordString = new DelimitedString(
                finRecordString,
                "##"
        );
    }

    @Override
    public double amount() {
        return wrapException(() ->
                Double.parseDouble(finRecordString.elementAt(1))
        );
    }

    @Override
    public LocalDate date() {
        return wrapException(() ->
                LocalDate.parse(finRecordString.elementAt(0))
        );
    }

    @Override
    public String classifier() {
        return finRecordString.elementAt(2);
    }

    private <T> T wrapException(Supplier<T> wrapped) {
        try {
            return wrapped.get();
        } catch(Throwable e) {
            throw new IllegalArgumentException(
                    "Unable to read data from FinRecord: %s".formatted(finRecordString),
                    e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinRecordOfString that = (FinRecordOfString) o;
        return finRecordString.equals(that.finRecordString);
    }

    @Override
    public int hashCode() {
        return Objects.hash(finRecordString);
    }

    @Override
    public String toString() {
        return finRecordString.toString();
    }
}
