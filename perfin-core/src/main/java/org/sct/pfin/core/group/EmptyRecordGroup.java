package org.sct.pfin.core.group;

import org.sct.pfin.core.FinRecord;
import org.sct.pfin.core.RecordGroup;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class EmptyRecordGroup implements RecordGroup {

    @Override
    public Iterator<FinRecord> iterator() {
        return Collections.emptyIterator();
    }

    @Override
    public Iterable<String> classifiers() {
        return Collections.emptyList();
    }

    @Override
    public Iterable<LocalDate> dates() {
        return Collections.emptyList();
    }

    @Override
    public Stream<FinRecord> asStream() {
        return Stream.empty();
    }

    @Override
    public RecordGroup filter(Predicate<FinRecord> condition) {
        return this;
    }
}
