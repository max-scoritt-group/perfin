package org.sct.pfin.core.group;

import org.sct.pfin.core.FinRecord;
import org.sct.pfin.core.record.FinRecordOfString;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Predicate;

/**
 * Fin records read from file based on provided condition
 */
public class FilteredFileRecords implements Iterable<FinRecord> {
    private final File file;
    private final Predicate<FinRecord> finRecordCondition;

    public FilteredFileRecords(final File file, final Predicate<FinRecord> finRecordCondition) {
        this.file = file;
        this.finRecordCondition = finRecordCondition;
    }

    @Override
    public Iterator<FinRecord> iterator() {
        return new Iterator<>() {

            private final FinRecordFileScannerWithFilter scanner;

            {
                try {
                    scanner = new FinRecordFileScannerWithFilter(file, finRecordCondition);
                } catch (FileNotFoundException e) {
                    throw new IllegalArgumentException("Cannot parse/find record file " + file.getAbsolutePath(), e);
                }
            }

            @Override
            public boolean hasNext() {
                return scanner.hasNext();
            }

            @Override
            public FinRecord next() {
                return scanner.next();
            }
        };
    }

    /**
     * Scan file for fin records matching given condition
     */
    private static final class FinRecordFileScannerWithFilter {
        private final Predicate<FinRecord> recordPredicate;
        private final Scanner scanner;
        private final File file;

        private ScanningState scanningState;
        private FinRecord finRecord;

        private FinRecordFileScannerWithFilter(File file, Predicate<FinRecord> recordPredicate) throws FileNotFoundException {
            this.file = file;
            this.recordPredicate = recordPredicate;

            this.scanner = new Scanner(file);
            this.scanningState = ScanningState.NO_SEARCH_ATTEMPTED;
        }

        public boolean hasNext() {
            if(scanningState.noSearchAttempted()) {
                try {
                    finRecord = new NextFilteredFromScanner(
                            scanner,
                            recordPredicate)
                            .value();
                    scanningState = ScanningState.HAS_MORE_RECORDS;
                } catch (NoSuchElementException e) {
                    scanningState = ScanningState.NO_MORE_RECORDS;
                }
            }
            return scanningState.hasNext();
        }

        public FinRecord next() {
            if(!hasNext()) {
                throw new NoSuchElementException("No more elements to return from file %s for predicate %s".formatted(file, recordPredicate));
            }
            scanningState = ScanningState.NO_SEARCH_ATTEMPTED;
            return finRecord;
        }

        /**
         * Next available fin record from a scanner matching provided condition
         */
        private static final class NextFilteredFromScanner {
            private final Scanner scanner;
            private final Predicate<FinRecord> recordPredicate;

            private NextFilteredFromScanner(Scanner scanner, Predicate<FinRecord> recordPredicate) {
                this.scanner = scanner;
                this.recordPredicate = recordPredicate;
            }

            public FinRecord value() {
                while (scanner.hasNextLine()) {
                    var nextFinRecord = new FinRecordOfString(
                            scanner.nextLine());
                    if (recordPredicate.test(nextFinRecord)) {
                        return nextFinRecord;
                    }
                }

               throw new NoSuchElementException("No more elements for predicate %s".formatted(recordPredicate));
            }
        }

        /**
         * State for scanning iterations
         */
        private enum ScanningState {
            NO_SEARCH_ATTEMPTED(false, false),
            NO_MORE_RECORDS(true, false),
            HAS_MORE_RECORDS(true, true);

            private final boolean searchAttempted;
            private final boolean hasNext;

            ScanningState(boolean searchAttempted, boolean hasNext) {
                this.searchAttempted = searchAttempted;
                this.hasNext = hasNext;
            }

            public boolean noSearchAttempted() {
                return !searchAttempted;
            }

            public boolean hasNext() {
                return hasNext;
            }
        }
    }


}
