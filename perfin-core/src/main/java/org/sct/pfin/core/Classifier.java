package org.sct.pfin.core;

/**
 * Classifier which is part of hierarchy: <code>name:p1.p2.p3...</code>
 */
public interface Classifier {
    String name();
    int depth();
    String path();
    Classifier parent();
}
