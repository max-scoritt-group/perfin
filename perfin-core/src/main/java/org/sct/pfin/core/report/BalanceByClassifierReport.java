package org.sct.pfin.core.report;

import org.sct.pfin.core.FinRecord;
import org.sct.pfin.core.FinReport;
import org.sct.pfin.core.RecordGroup;
import org.sct.pfin.core.util.StreamOfIterable;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Produces result in form of map <code>[classifier] -> [balance]</code>
 */
public class BalanceByClassifierReport implements FinReport<Map<String, BigDecimal>> {
    private final RecordGroup records;

    public BalanceByClassifierReport(RecordGroup records) {
        this.records = records;
    }

    @Override
    public Map<String, BigDecimal> prepare() {
        return new StreamOfIterable<>(records).get()
                .collect(
                        new FinRecordSumBy<>(FinRecord::classifier)
                );
    }
}
