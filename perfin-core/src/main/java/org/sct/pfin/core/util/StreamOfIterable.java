package org.sct.pfin.core.util;

import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class StreamOfIterable<T> implements Supplier<Stream<T>> {
    private final Iterable<T> iterable;

    public StreamOfIterable(Iterable<T> iterable) {
        this.iterable = iterable;
    }

    @Override
    public Stream<T> get()  {
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
