package org.sct.pfin.core.group;

import org.sct.pfin.core.FinRecord;
import org.sct.pfin.core.RecordGroup;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class SingleRecordGroup implements RecordGroup {

    private final FinRecord source;

    public SingleRecordGroup(FinRecord source) {
        this.source = source;
    }

    @Override
    public Iterable<String> classifiers() {
        return List.of(source.classifier());
    }

    @Override
    public Iterable<LocalDate> dates() {
        return List.of(source.date());
    }

    @Override
    public Stream<FinRecord> asStream() {
        return Stream.of(source);
    }

    @Override
    public RecordGroup filter(Predicate<FinRecord> condition) {
        if(condition.test(source)) {
            return this;
        } else {
            return new EmptyRecordGroup();
        }
    }

    @Override
    public Iterator<FinRecord> iterator() {
        return List.of(source).iterator();
    }
}
