package org.sct.pfin.core.report;

import org.sct.pfin.core.FinReport;
import org.sct.pfin.core.RecordGroup;
import org.sct.pfin.core.util.StreamOfIterable;

import java.math.BigDecimal;

public class BalanceFinReport implements FinReport<Double> {

    private final RecordGroup records;

    public BalanceFinReport(RecordGroup records)  {
        this.records = records;
    }

    @Override
    public Double prepare() {
        return new StreamOfIterable<>(records).get()
                .map(r -> BigDecimal.valueOf(r.amount()))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO)
                .doubleValue();
    }
}
