package org.sct.pfin.core;

public interface FinReport<T> {
    T prepare();
}
