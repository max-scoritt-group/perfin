package org.sct.pfin.core.util;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Split string into elements using provided delimiter.
 * <br/>Successive calls to {@link #elementAt} and {@link #concatAfter} will not cause parsing.
 * <br/>Not thread-safe.
 */
public class DelimitedString {
        private final String source;
        private final String delimiter;

        private final CachingSupplier<List<String>> split;

        public DelimitedString(String source, String delimiterRegex) {
            this.source = source;
            this.delimiter = delimiterRegex;
            this.split = new CachingSupplier<>(
                    () -> List.of(source.split(delimiterRegex)));
        }

        public String elementAt(int index) {
            if(split.get().size() <= index) {
                return "";
            } else {
                return split.get().get(index);
            }
        }

        public String concatAfter(int index) {
            return split.get().stream()
                    .skip(index)
                    .collect(Collectors.joining(delimiter));
        }

        @Override
        public String toString() {
            return source;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DelimitedString that = (DelimitedString) o;
            return source.equals(that.source);
        }

        @Override
        public int hashCode() {
            return Objects.hash(source);
        }
}
