package org.sct.pfin.core;

import java.time.LocalDate;

public interface FinRecord {
    double amount();
    LocalDate date();
    String classifier();
}
