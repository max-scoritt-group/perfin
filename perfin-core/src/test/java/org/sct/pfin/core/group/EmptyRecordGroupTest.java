package org.sct.pfin.core.group;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class EmptyRecordGroupTest {

    @Test
    public void classifiersOfEmptyRecordsTest() {
        assertIterableEquals(
                Collections.EMPTY_LIST,
                new EmptyRecordGroup().classifiers()
        );
    }

}