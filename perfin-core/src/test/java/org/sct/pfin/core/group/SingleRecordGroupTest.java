package org.sct.pfin.core.group;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.record.FinRecordOfString;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

class SingleRecordGroupTest {

    @Test
    public void classifiersOfAGroup()  {
        assertIterableEquals(
                List.of("abc"),
                new SingleRecordGroup(
                        new FinRecordOfString(
                                "2022-05-07##100.123##abc"
                        )
                ).classifiers()
        );
    }

    @Test
    public void iteratorOfAGroup()  {
        assertEquals(
                new FinRecordOfString(
                        "2022-05-07##100.123##abc"
                ),
                new SingleRecordGroup(
                        new FinRecordOfString(
                                "2022-05-07##100.123##abc"
                        )
                ).iterator().next()
        );
    }

}