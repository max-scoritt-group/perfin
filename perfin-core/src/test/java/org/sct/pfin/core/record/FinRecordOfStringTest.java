package org.sct.pfin.core.record;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FinRecordOfStringTest {

    @Test
    void parseCorrectAmountTest() {
        assertEquals(
                100.123D,
                new FinRecordOfString("2022-05-06##100.123")
                        .amount()
        );
    }

    @Test
    void parseCorrectDateTest() {
        assertEquals(
                LocalDate.of(2022, 5, 6),
                new FinRecordOfString("2022-05-06##100.123")
                        .date()
        );
    }

    @Test
    void parseCorrectClassifiersTest() {
        assertEquals(
                "abc.abcd",
                new FinRecordOfString("2022-05-06##100.123##abc.abcd")
                        .classifier()
        );
    }

    @Test
    void parseEmptyClassifierTest()  {
        assertEquals(
                "",
                new FinRecordOfString("2022-05-06##100.123")
                        .classifier()
        );
    }

    @Test
    void exceptionOnWrongDateTest()  {
        assertThrows(
                IllegalArgumentException.class,
                () -> new FinRecordOfString("2022--05-06##100.123").date()
        );
    }

    @Test
    void exceptionOnWrongAmountTest() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new FinRecordOfString("2022-05-06##100..123").amount()
        );
    }

}