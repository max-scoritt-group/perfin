package org.sct.pfin.core.group;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.record.FinRecordOfString;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RecordGroupOfIterableTest {

    @Test
    public void classifiersOfAGroup() {
        assertIterableEquals(
                List.of("abc", "def", "ghi"),
                new RecordsOfIterable(
                        new FinRecordOfString("2022-05-08##100.123##abc"),
                        new FinRecordOfString("2022-05-07##100.123##def"),
                        new FinRecordOfString("2022-05-09##100.123##ghi")
                ).classifiers()
        );
    }

    @Test
    public void datesOfAGroup() {
        assertIterableEquals(
                List.of(
                        LocalDate.parse("2022-05-08"),
                        LocalDate.parse("2022-05-09"),
                        LocalDate.parse("2022-05-10"),
                        LocalDate.parse("2022-05-11")
                ),
                new RecordsOfIterable(
                        new FinRecordOfString("2022-05-11##100.123##abc"),
                        new FinRecordOfString("2022-05-08##100.123##def"),
                        new FinRecordOfString("2022-05-10##100.123##ghi"),
                        new FinRecordOfString("2022-05-09##100.123##ghi")
                ).dates()
        );
    }


    @Test
    public void iteratorOfAGroup() {
        assertIterableEquals(
                List.of(
                        new FinRecordOfString("2022-05-08##100.123##abc"),
                        new FinRecordOfString("2022-05-07##100.123##def"),
                        new FinRecordOfString("2022-05-09##100.123##ghi")
                ),
                new RecordsOfIterable(
                        new FinRecordOfString("2022-05-08##100.123##abc"),
                        new FinRecordOfString("2022-05-07##100.123##def"),
                        new FinRecordOfString("2022-05-09##100.123##ghi")

                )
        );
    }
}