package org.sct.pfin.core.group;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.record.FinRecordOfString;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

class DateFilterRecordGroupTest {
    @Test
    public void filterRecordsTest() {
        assertIterableEquals(
                new RecordsOfIterable(
                        new FinRecordOfString("2022-05-09##103"),
                        new FinRecordOfString("2022-05-09##104"),
                        new FinRecordOfString("2022-05-09##105")
                ),
                new DateFilteredFinRecords(
                        LocalDate.parse("2022-05-09"),
                        new RecordsOfIterable(
                                new FinRecordOfString("2022-05-02##101"),
                                new FinRecordOfString("2022-05-02##102"),
                                new FinRecordOfString("2022-05-09##103"),
                                new FinRecordOfString("2022-05-09##104"),
                                new FinRecordOfString("2022-05-09##105")
                        )
                )
        );
    }
}