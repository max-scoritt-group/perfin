package org.sct.pfin.core.group;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.record.FinRecordOfString;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FilteredFileRecordsTest {
    @Test
    public void filterFileRecordsByDate() {
        assertIterableEquals(
                List.of(
                        new FinRecordOfString("2022-01-07##100.125##abc5")
                ),
                new FilteredFileRecords(
                        new File("src/test/resources/sample1.txt"),
                        (fr) -> LocalDate.parse("2022-01-07").equals(fr.date())
                )
        );
    }

    @Test
    public void missingFileParsingTest() {
        assertThrows(Throwable.class,
                () -> new FilteredFileRecords(
                        new File("src/test/resources/samp1.txt"),
                        (fr) -> LocalDate.parse("2022-01-07").equals(fr.date())
                ).iterator()
        );
    }
}