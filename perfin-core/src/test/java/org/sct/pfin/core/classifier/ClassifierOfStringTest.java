package org.sct.pfin.core.classifier;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.Classifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ClassifierOfStringTest {

    @Test
    void correctNameTest() {
        assertEquals(
                "name",
                new ClassifierOfString("name:p1.p2.p3")
                        .name()
        );
    }

    @Test
    void correctDepthTest() {
        assertEquals(
                3,
                new ClassifierOfString("name:p1.p2.p3")
                        .depth()
        );
    }

    @Test
    void correctPathTest() {
        assertEquals(
                "p1.p2.p3",
                new ClassifierOfString("name:p1.p2.p3")
                        .path()
        );
    }

    @Test
    void correctParentTest() {
        assertEquals(
                new ClassifierOfString("name:p1.p2"),
                new ClassifierOfString("name:p1.p2.p3")
                        .parent()
        );
    }

    @Test
    void nameOnlyTest() {
        assertEquals(
                "",
                new ClassifierOfString("name:")
                        .path()
        );
    }

    @Test
    void invalidNameTest()  {
        assertThrows(
                IllegalArgumentException.class,
                new ClassifierOfString("name::")::name
        );
    }

    @Test
    void invalidPathTest()  {
        assertThrows(
                IllegalArgumentException.class,
                new ClassifierOfString("name:p1.")::path
        );
    }

    @Test
    void parentOfEmptyPathTest()  {
        assertEquals(
                new ClassifierOfString("name:"),
                new ClassifierOfString("name:")
                        .parent()
        );
    }

    @Test
    void parentOfDepthOneTest()  {
        assertEquals(
                new ClassifierOfString("name:"),
                new ClassifierOfString("name:abc")
                        .parent()
        );
    }

}