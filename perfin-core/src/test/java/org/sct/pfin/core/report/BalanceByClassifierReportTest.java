package org.sct.pfin.core.report;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.group.RecordsOfIterable;
import org.sct.pfin.core.record.FinRecordOfString;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BalanceByClassifierReportTest {
    @Test
    public void balanceTest() {
        assertMapEquals(
            Map.of(
                    "abc", BigDecimal.valueOf(100.123),
                    "def", BigDecimal.valueOf(200.001),
                    "ghi", BigDecimal.valueOf(300.003)
            ),
            new BalanceByClassifierReport(
                    new RecordsOfIterable(
                            List.of(
                                    new FinRecordOfString("2022-05-10##50.100##abc"),
                                    new FinRecordOfString("2022-05-10##101.0##def"),
                                    new FinRecordOfString("2022-05-10##400.005##ghi"),
                                    new FinRecordOfString("2022-05-10##-100.002##ghi"),
                                    new FinRecordOfString("2022-05-10##99.001##def"),
                                    new FinRecordOfString("2022-05-10##50.023##abc")
                            )
                    )
            ).prepare()
        );
    }

    private static <K, V> void assertMapEquals(Map<K,V> expected, Map<K,V> actual) {
        assertEquals(
                expected.size(),
                actual.size()
        );

        expected.keySet().forEach(k ->
                assertEquals(
                        expected.get(k),
                        actual.get(k),
                        String.format("Expected %s for key %s, but was %s", expected.get(k), k, actual.get(k))
                )
        );
    }

}