package org.sct.pfin.core;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapMatcher {
    public <K, V> void assertMapEquals(Map<K,V> expected, Map<K,V> actual) {
        assertEquals(
                expected.size(),
                actual.size()
        );

        expected.keySet().forEach(k ->
                assertEquals(
                        expected.get(k),
                        actual.get(k),
                        String.format("Expected %s for key %s, but was %s", expected.get(k), k, actual.get(k))
                )
        );
    }
}
