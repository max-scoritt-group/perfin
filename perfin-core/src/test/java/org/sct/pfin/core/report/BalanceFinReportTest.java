package org.sct.pfin.core.report;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.group.EmptyRecordGroup;
import org.sct.pfin.core.group.RecordsOfIterable;
import org.sct.pfin.core.record.FinRecordOfString;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BalanceFinReportTest {

    @Test
    public void balanceReportTest() {
        assertEquals(
                100.123,
                new BalanceFinReport(
                        new RecordsOfIterable(
                                List.of(
                                    new FinRecordOfString("2022-05-08##100.100"),
                                    new FinRecordOfString("2022-05-09##200.020"),
                                    new FinRecordOfString("2022-05-10##300.003"),
                                    new FinRecordOfString("2022-05-11##-500.000"),
                                    new FinRecordOfString("2022-05-12##0.001"),
                                    new FinRecordOfString("2022-05-13##-0.001")
                                )
                        )
                ).prepare()
        );
    }

    @Test
    public void emptyRecordsBalanceReport() {
        assertEquals(
                0.0,
                new BalanceFinReport(
                        new EmptyRecordGroup()
                ).prepare()
        );
    }
}