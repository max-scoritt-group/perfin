package org.sct.pfin.core.group;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.sct.pfin.core.record.FinRecordOfString;

import java.io.File;
import java.util.Collections;
import java.util.List;

class FileRecordsTest {
    @Test
    public void fileGroupTest() {
        Assertions.assertIterableEquals(
                List.of(
                        new FinRecordOfString("2022-01-01##100.123##abc 100"),
                        new FinRecordOfString("2022-01-01##100.124##abc2"),
                        new FinRecordOfString("2022-01-07##100.125##abc5"),
                        new FinRecordOfString("2022-01-04##100.126##abc3"),
                        new FinRecordOfString("2022-01-05##100.127##abc2")
                ),
                new FileRecords(
                        new File("src/test/resources/sample1.txt")
                )
        );
    }

    @Test
    public void emptyFileGroupTest() {
        Assertions.assertIterableEquals(
                Collections.EMPTY_LIST,
                new FileRecords(
                        new File("src/test/resources/empty.txt")
                )
        );
    }
}