package org.sct.pfin.core.group;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.RecordGroup;
import org.sct.pfin.core.record.FinRecordOfString;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ClassifierFilteredRecordsTest {
    @Test
    public void filteredGroupTest() {
        assertIterableEquals(
                new RecordsOfIterable(
                        new FinRecordOfString("2022-01-09##500##abc"),
                        new FinRecordOfString("2022-01-02##300##abc"),
                        new FinRecordOfString("2022-01-03##100##abc")
                ),
                new ClassifierFilteredRecords(
                        "abc",
                        new RecordsOfIterable(
                                new FinRecordOfString("2022-01-09##500##abc"),
                                new FinRecordOfString("2022-01-09##500##def"),
                                new FinRecordOfString("2022-01-02##300##abc"),
                                new FinRecordOfString("2022-01-20##-100##ab"),
                                new FinRecordOfString("2022-01-21##-200##a"),
                                new FinRecordOfString("2022-01-03##100##abc")
                        )
                )
        );
    }

    @Test
    public void correctDatesTest() {
        assertIterableEquals(
                List.of(
                        LocalDate.of(2022, 1, 2),
                        LocalDate.of(2022, 1, 3),
                        LocalDate.of(2022, 1, 9)
                ),
                new ClassifierFilteredRecords(
                        "abc",
                        new RecordsOfIterable(
                                new FinRecordOfString("2022-01-09##500##abc"),
                                new FinRecordOfString("2022-01-09##500##def"),
                                new FinRecordOfString("2022-01-02##300##abc"),
                                new FinRecordOfString("2022-01-20##-100##ab"),
                                new FinRecordOfString("2022-01-21##-200##a"),
                                new FinRecordOfString("2022-01-03##100##abc")
                        )
                ).dates()
        );
    }

    @Test
    public void correctClassifiersTest() {
        assertIterableEquals(
                List.of(
                        "abc"
                ),
                new ClassifierFilteredRecords(
                        "abc",
                        new RecordsOfIterable(
                                new FinRecordOfString("2022-01-09##500##abc"),
                                new FinRecordOfString("2022-01-09##500##def"),
                                new FinRecordOfString("2022-01-02##300##abc"),
                                new FinRecordOfString("2022-01-20##-100##ab"),
                                new FinRecordOfString("2022-01-21##-200##a"),
                                new FinRecordOfString("2022-01-03##100##abc")
                        )
                ).classifiers()
        );
    }

    @Test
    public void filterTest() {
        assertIterableEquals(
                new RecordsOfIterable(
                        new FinRecordOfString("2022-01-03##100##abc")
                ),
                new ClassifierFilteredRecords(
                        "abc",
                        new RecordsOfIterable(
                                new FinRecordOfString("2022-01-09##500##abc"),
                                new FinRecordOfString("2022-01-09##500##def"),
                                new FinRecordOfString("2022-01-02##300##abc"),
                                new FinRecordOfString("2022-01-20##-100##ab"),
                                new FinRecordOfString("2022-01-21##-200##a"),
                                new FinRecordOfString("2022-01-03##100##abc")
                        )
                ).filter(r -> r.date().equals(LocalDate.of(2022, 1, 3)))
        );
    }


}