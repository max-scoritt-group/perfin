package org.sct.pfin.core.report;

import org.junit.jupiter.api.Test;
import org.sct.pfin.core.MapMatcher;
import org.sct.pfin.core.group.RecordsOfIterable;
import org.sct.pfin.core.record.FinRecordOfString;

import java.math.BigDecimal;
import java.time.Month;
import java.util.Map;

class MonthToMonthReportTest {

    @Test
    public void monthToMonthReportTest() {
        new MapMatcher().assertMapEquals(
                Map.of(
                        Month.MAY, BigDecimal.valueOf(50.100),
                        Month.JUNE, BigDecimal.valueOf(-100.002)
                ),
                new MonthToMonthReport(
                        new RecordsOfIterable(
                                new FinRecordOfString("2022-05-09##50.100##abc"),
                                new FinRecordOfString("2022-05-10##101.0##def"),
                                new FinRecordOfString("2022-05-11##400.005##ghi"),
                                new FinRecordOfString("2022-06-09##-100.002##ghi"),
                                new FinRecordOfString("2022-06-11##99.001##def"),
                                new FinRecordOfString("2022-07-15##50.023##abc")
                        ),
                        10
                ).prepare()
        );
    }

}